<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//user authentication
Route::post('login', 'Api\Auth\AuthController@login');
Route::post('register', 'Api\Auth\AuthController@register');
Route::post('forgot-password', 'Api\Auth\AuthController@forgotPassword');
Route::post('reset-password', 'Api\Auth\AuthController@resetPassword');

Route::group(['middleware' => ['auth:api']], function () {
  //employee management
  Route::group(['prefix' => 'employee'], function () {
    Route::post('create', 'Api\Employee\CrudController@create');
    Route::post('update/{id}', 'Api\Employee\CrudController@update');
    Route::delete('delete/{id}', 'Api\Employee\CrudController@delete');
    Route::post('list', 'Api\Employee\GetDataController@getList');
    Route::post('list-paginate', 'Api\Employee\GetDataController@getListPaginate');
  });
});
