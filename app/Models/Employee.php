<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Employee extends Model
{
  use HasFactory, SoftDeletes;

  public function profile()
  {
    return $this->hasOne('App\Models\Profile', 'employee_id', 'id');
  }

  public function user()
  {
    return $this->belongsTo('App\Models\User', 'user_create_id', 'id');
  }
}
