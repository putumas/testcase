<?php

namespace App\Http\Controllers\Api\Employee;

use App\Http\Controllers\Controller;
use App\Http\Resources\EmployeeCollection;
use App\Models\Employee;
use Illuminate\Http\Request;

class GetDataController extends Controller
{
  public function query($req)
  {
    $query = Employee::when($req['name'], function ($q) use ($req) {
      return $q->where('name', 'like', '%' . $req['name'] . '%');
    })->when($req['email'], function ($q) use ($req) {
      return $q->where('email', 'like', '%' . $req['email'] . '%');
    })->when($req['division'], function ($q) use ($req) {
      return $q->where('division', 'like', '%' . $req['division'] . '%');
    })->when($req['address'], function ($q) use ($req) {
      return $q->whereHas('profile', function ($q) use ($req) {
        $q->where('address', 'like', '%' . $req['address'] . '%');
      });
    })->when($req['gender'], function ($q) use ($req) {
      return $q->whereHas('profile', function ($q) use ($req) {
        $q->where('gender', $req['gender']);
      });
    })->when($req['birth_start'] && $req['birth_start'], function ($q) use ($req) {
      return $q->whereHas('profile', function ($q) use ($req) {
        $q->whereBetween('birth_date', [date("Y-m-d", strtotime($req['birth_start'])), date("Y-m-d", strtotime($req['birth_end'] . ' + 1 days'))]);
      });
    });

    return $query;
  }

  public function getList(Request $request)
  {
    $request->validate([
      'name' => 'nullable|string',
      'email' => 'nullable|string',
      'division' => 'nullable|string',
      'birth_start' => 'nullable|date',
      'birth_end' => 'nullable|date',
      'address' => 'nullable|string',
      'gender' => 'nullable|string'
    ]);

    $req = $request->all();

    if (($req['birth_start'] && !$req['birth_end']) || (!$req['birth_start'] && $req['birth_end'])) {
      return response()->json(['status' => 'failed', 'message' => 'pencarian tanggal gagal, birth_start dan birth_end harus terisi!'], 403);
    } else {
      $query = $this->query($req)->get();
      $employee = new EmployeeCollection($query);

      return response()->json(['status' => 'success', 'return' => $employee], 200);
    }
  }

  public function getListPaginate(Request $request)
  {
    $request->validate([
      'paginate' => 'required|integer|min:5',
      'name' => 'nullable|string',
      'email' => 'nullable|string',
      'division' => 'nullable|string',
      'birth_start' => 'nullable|date',
      'birth_end' => 'nullable|date',
      'address' => 'nullable|string',
      'gender' => 'nullable|string'
    ]);

    $req = $request->all();

    if (($req['birth_start'] && !$req['birth_end']) || (!$req['birth_start'] && $req['birth_end'])) {
      return response()->json(['status' => 'failed', 'message' => 'pencarian tanggal gagal, birth_start dan birth_end harus terisi!'], 403);
    } else {
      $employee = $this->query($req)->with('profile')->paginate($request->paginate);
      return response()->json(['status' => 'success', 'return' => $employee], 200);
    }
  }
}
