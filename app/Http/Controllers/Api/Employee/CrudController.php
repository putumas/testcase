<?php

namespace App\Http\Controllers\Api\Employee;

use App\Http\Controllers\Controller;
use App\Models\Employee;
use App\Models\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CrudController extends Controller
{
  public function create(Request $request)
  {
    $request->validate([
      'name' => 'required',
      'email' => 'required|unique:employees|email',
      'division' => 'required',
      'birth_date' => 'required|date',
      'gender' => 'required',
      'address' => 'required'
    ]);

    DB::beginTransaction();
    $employee = new Employee();
    $employee->name = $request->name;
    $employee->email = $request->email;
    $employee->division = $request->division;
    $employee->user_create_id = Auth::user()->id;
    $employee->save();

    $profile = new Profile();
    $profile->employee_id =  $employee->id;
    $profile->birth_date = $request->birth_date;
    $profile->gender = $request->gender;
    $profile->address = $request->address;
    $profile->save();
    DB::commit();

    return response()->json(['status' => 'success', 'message' => 'berhasil menambahkan employee!'], 201);
  }

  public function update(Request $request, $id = null)
  {
    $request->validate([
      'name' => 'required',
      'email' => 'required|email',
      'division' => 'required',
      'birth_date' => 'required|date',
      'gender' => 'required',
      'address' => 'required'
    ]);

    $employee = Employee::where('id', $id)->first();
    if ($employee) {
      DB::beginTransaction();
      $employee->name = $request->name;
      $employee->email = $request->email;
      $employee->division = $request->division;
      $employee->save();

      Profile::where('employee_id', $employee->id)->update([
        'birth_date' => $request->birth_date,
        'gender' => $request->gender,
        'address' => $request->address
      ]);
      DB::commit();

      return response()->json(['status' => 'success', 'message' => 'berhasil melakukan update!'], 201);
    } else {
      return response()->json(['status' => 'failed', 'message' => 'data employee tidak ditemukan!'], 403);
    }
  }

  public function delete($id = null)
  {
    $employee = Employee::where('id', $id)->first();
    if ($employee) {
      $employee->delete();
      return response()->json(['status' => 'success', 'message' => 'berhasil menghapus data!'], 200);
    } else {
      return response()->json(['status' => 'failed', 'message' => 'data employee tidak ditemukan!'], 403);
    }
  }
}
