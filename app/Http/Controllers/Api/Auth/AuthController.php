<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Models\ResetPassword;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
  public function login(Request $request)
  {
    $request->validate([
      'email' => 'required|string|email',
      'password' => 'required|string'
    ]);

    if (Auth::attempt(['email' => strtolower($request->email), 'password' => $request->password])) {
      $user = Auth::user();
      $data['token'] =  $user->createToken('nApp')->accessToken;
      $data['name'] = $user->name;
      $data['email'] = $user->email;
      return response()->json(['status' => 'success', 'return' => $data], 200);
    } else {
      return response()->json(['status' => 'failed', 'return' => 'Username / Password Salah!'], 401);
    }
  }

  public function register(Request $request)
  {
    $request->validate([
      'name' => 'required|string',
      'email' => 'required|string|unique:users|email',
      'password' => 'required|string|min:6'
    ]);

    $user = new User();
    $user->name = $request->name;
    $user->email = $request->email;
    $user->password = bcrypt($request->password);
    $user->save();

    return response()->json(['status' => 'success', 'message' => 'berhasil registrasi!'], 201);
  }

  public function forgotPassword(Request $request)
  {
    $request->validate([
      'email' => 'required|string|email'
    ]);

    $user = User::where('email', $request->email)->first();
    if ($user) {
      $resetPass = new ResetPassword();
      $resetPass->user_id = $user->id;
      $resetPass->reset_code = bin2hex(random_bytes(3));
      $resetPass->save();
      return response()->json(['status' => 'success', 'return' => ['reset_code' => $resetPass->reset_code]], 201);
    } else {
      return response()->json(['status' => 'failed', 'message' => 'email tidak ditemukan!'], 403);
    }
  }

  public function resetPassword(Request $request)
  {
    $request->validate([
      'email' => 'required|string|email',
      'reset_code' => 'required|string',
      'new_password' => 'required|string|min:6'
    ]);

    $resCode = ResetPassword::whereHas('user', function ($q) use ($request) {
      $q->where('email', $request->email);
    })->where('reset_code', $request->reset_code)->where('is_used', 0)->first();

    if ($resCode) {
      User::where('email', $request->email)->update([
        'password' => bcrypt($request->new_password)
      ]);
      $resCode->is_used = 1;
      $resCode->save();

      return response()->json(['status' => 'success', 'message' => 'sukses melakukan reset password!'], 200);
    } else {
      return response()->json(['status' => 'failed', 'message' => 'kode tidak valid!'], 403);
    }
  }
}
