<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class EmployeeCollection extends ResourceCollection
{
  /**
   * Transform the resource collection into an array.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return array
   */
  public function toArray($request)
  {
    return $this->collection->map(function ($data) {
      return [
        'id' => $data->id,
        'name' => $data->name,
        'email' => $data->email,
        'division' => $data->division,
        'user_create_id' => $data->user_create_id,
        'user_create_name' => $data->user->name,
        'profile' => $data->profile
      ];
    });
    // return parent::toArray($request);
  }
}
