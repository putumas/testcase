<?php

namespace Database\Seeders;

use App\Models\Employee;
use Illuminate\Database\Seeder;

class EmployeeSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    Employee::insert([
      [
        'name' => 'employee 1',
        'email' => 'employee1@email.com',
        'division' => 'IT',
        'user_create_id' => 1,
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
      ],
      [
        'name' => 'employee 2',
        'email' => 'employee2@email.com',
        'division' => 'IT',
        'user_create_id' => 1,
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
      ],
      [
        'name' => 'employee 3',
        'email' => 'employee3@email.com',
        'division' => 'IT',
        'user_create_id' => 1,
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
      ],
      [
        'name' => 'employee 4',
        'email' => 'employee4@email.com',
        'division' => 'IT',
        'user_create_id' => 1,
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
      ],
      [
        'name' => 'employee 5',
        'email' => 'employee5@email.com',
        'division' => 'IT',
        'user_create_id' => 1,
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
      ],
      [
        'name' => 'employee 6',
        'email' => 'employee6@email.com',
        'division' => 'IT',
        'user_create_id' => 1,
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
      ],
      [
        'name' => 'employee 7',
        'email' => 'employee7@email.com',
        'division' => 'IT',
        'user_create_id' => 1,
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
      ],
      [
        'name' => 'employee 8',
        'email' => 'employee8@email.com',
        'division' => 'IT',
        'user_create_id' => 1,
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
      ],
      [
        'name' => 'employee 9',
        'email' => 'employee9@email.com',
        'division' => 'IT',
        'user_create_id' => 1,
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
      ],
      [
        'name' => 'employee 10',
        'email' => 'employee10@email.com',
        'division' => 'IT',
        'user_create_id' => 1,
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
      ],
      [
        'name' => 'employee 11',
        'email' => 'employee11@email.com',
        'division' => 'IT',
        'user_create_id' => 1,
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
      ],
      [
        'name' => 'employee 12',
        'email' => 'employee12@email.com',
        'division' => 'IT',
        'user_create_id' => 1,
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
      ],
      [
        'name' => 'employee 13',
        'email' => 'employee13@email.com',
        'division' => 'IT',
        'user_create_id' => 1,
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
      ],
      [
        'name' => 'employee 14',
        'email' => 'employee14@email.com',
        'division' => 'IT',
        'user_create_id' => 1,
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
      ],
      [
        'name' => 'employee 15',
        'email' => 'employee15@email.com',
        'division' => 'IT',
        'user_create_id' => 1,
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
      ],
    ]);
  }
}
