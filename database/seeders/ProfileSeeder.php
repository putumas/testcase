<?php

namespace Database\Seeders;

use App\Models\Profile;
use Illuminate\Database\Seeder;

class ProfileSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    Profile::insert([
      [
        'employee_id' => 1,
        'birth_date' => date('Y-m-d'),
        'address' => 'Denpasar',
        'gender' => 'male',
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
      ],
      [
        'employee_id' => 2,
        'birth_date' => date('Y-m-d'),
        'address' => 'Denpasar',
        'gender' => 'female',
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
      ],
      [
        'employee_id' => 3,
        'birth_date' => date('Y-m-d'),
        'address' => 'Denpasar',
        'gender' => 'male',
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
      ],
      [
        'employee_id' => 4,
        'birth_date' => date('Y-m-d'),
        'address' => 'Denpasar',
        'gender' => 'male',
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
      ],
      [
        'employee_id' => 5,
        'birth_date' => date('Y-m-d'),
        'address' => 'Denpasar',
        'gender' => 'female',
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
      ],
      [
        'employee_id' => 6,
        'birth_date' => date('Y-m-d'),
        'address' => 'Denpasar',
        'gender' => 'male',
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
      ],
      [
        'employee_id' => 7,
        'birth_date' => date('Y-m-d'),
        'address' => 'Denpasar',
        'gender' => 'female',
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
      ],
      [
        'employee_id' => 8,
        'birth_date' => date('Y-m-d'),
        'address' => 'Denpasar',
        'gender' => 'male',
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
      ],
      [
        'employee_id' => 9,
        'birth_date' => date('Y-m-d'),
        'address' => 'Denpasar',
        'gender' => 'female',
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
      ],
      [
        'employee_id' => 10,
        'birth_date' => date('Y-m-d'),
        'address' => 'Denpasar',
        'gender' => 'male',
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
      ],
      [
        'employee_id' => 11,
        'birth_date' => date('Y-m-d'),
        'address' => 'Denpasar',
        'gender' => 'female',
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
      ],
      [
        'employee_id' => 12,
        'birth_date' => date('Y-m-d'),
        'address' => 'Denpasar',
        'gender' => 'male',
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
      ],
      [
        'employee_id' => 13,
        'birth_date' => date('Y-m-d'),
        'address' => 'Denpasar',
        'gender' => 'female',
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
      ],
      [
        'employee_id' => 14,
        'birth_date' => date('Y-m-d'),
        'address' => 'Denpasar',
        'gender' => 'male',
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
      ],
      [
        'employee_id' => 15,
        'birth_date' => date('Y-m-d'),
        'address' => 'Denpasar',
        'gender' => 'female',
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
      ],
    ]);
  }
}
